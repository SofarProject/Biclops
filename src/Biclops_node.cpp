#include <ros/ros.h>
#include <cstdio>          // for FILE defn
#include <iostream>         // for cout, etc
#include <time.h>           // for clock_t and clock
#include <list>
#include <math.h>
#include <string>
#include <cstring>
#include <std_srvs/Empty.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Int16.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>

using namespace std;
using namespace ros;

#include <Biclops.h>
#include <PMDUtils.h>
#include <PMD.h>


#define A 97
#define D 100
#define W 119
#define S 115
#define H 104
#define PI 3.14159265


#define ANGLE_STEP 1

char *config_path="../../../src/Biclops/BiclopsDefault.cfg";

// Defines which axes we want to use.
int axisMask=Biclops::PanMask
+ Biclops::TiltMask;;

// Pointers to each axis (populated once controller is initialized).
PMDAxisControl *panAxis = NULL;
PMDAxisControl *tiltAxis = NULL;

// THE interface to Biclops
Biclops biclops_obj;

double pan_pos_value=0;
double tilt_pos_value=0;

double pan_joint_pos=0;
double tilt_joint_pos=0;

PMDAxisControl::Profile panProfile,tiltProfile;


bool Homing(std_srvs::Empty::Request &req, std_srvs::Empty::Response  &res){
  
  ROS_INFO("Performing Homing Sequence");
  biclops_obj.HomeAxes(axisMask,true);
  
  pan_pos_value=0;
  tilt_pos_value=0;
  
  pan_joint_pos=0;
  tilt_joint_pos=0;
  
  return true;
}



//quaternion to ypr with threshold for singularities
 void quaternionToYPR(geometry_msgs::Quaternion & q, double &yaw, double &pitch, double &roll) {
	double test = q.x*q.y + q.z*q.w;
	if (test > 0.499) { // singularity at north pole
		yaw = 2 * atan2(q.x,q.w);
		pitch = M_PI/2;
		roll = 0;
		return;
	}
	if (test < -0.499) { // singularity at south pole
		yaw = -2 * atan2(q.x,q.w);
		pitch = - M_PI/2;
		roll = 0;
		return;
	}
    double sqx = q.x*q.x;
    double sqy = q.y*q.y;
    double sqz = q.z*q.z;
    yaw = atan2(2*q.y*q.w-2*q.x*q.z , 1 - 2*sqy - 2*sqz);
	pitch = asin(2*test);
	roll = atan2(2*q.x*q.w-2*q.y*q.z , 1 - 2*sqx - 2*sqz);
}

void oculusMotionCallback(geometry_msgs::Quaternion oculus_orientation){
    double roll, pitch, yaw;
    //oculus rift frame is different thas why yaw and pitch dont correspont the ones of the Biclops
    double pan, tilt;
    quaternionToYPR(oculus_orientation,yaw,pitch,roll);
    
    pan=-yaw;
    tilt=-roll;
    
    double panrev=(pan/(2*PI));
    double tiltrev=(tilt/(2*PI));
    

    
    ROS_INFO("Pan rev : %f", panrev);
    ROS_INFO("Tilt rev: %f", tiltrev);
    
    panProfile.pos=panrev;
    tiltProfile.pos=tiltrev;
    

}

int main (int argc, char *argv[]) {
  
  ros::init(argc,argv,"Biclops");
  ROS_INFO("node creation");
  ros::NodeHandle nh;
  ros::Rate loop_rate(10);
  
  ros::Subscriber key_sub;
  ros::Subscriber oculus_orientation;
  ros::Publisher joint_pub;
  ros::ServiceServer homing_service;
  sensor_msgs::JointState joint_state;
  
  
  // ROS_INFO("ARGV : %s ",argv[1]);

  argv[1]=config_path;
  
  ROS_INFO( "\n\nBasic Biclops Running Example\n\n" );
 
  //Subscribing
    //key_sub=nh.subscribe<std_msgs::Int16>("/key_typed",1,keyCallback);
    oculus_orientation = nh.subscribe<geometry_msgs::Quaternion>("/oculus/orientation",1,oculusMotionCallback);
    
    //Publishing
    joint_pub=nh.advertise<sensor_msgs::JointState>("joint_states",1);
    
    //Homing sequence Service
    homing_service =nh.advertiseService("homing_sequence",Homing);
    
    
  //Initializing
 
  cout<<"Initialization Routine"<<biclops_obj.Initialize(argv[1])<<endl;
  
  if (!biclops_obj.Initialize(argv[1]))
  {
    
    ROS_INFO("Failed to open Biclops Communication\n");
    ROS_INFO("Press Enter key to terminate...\n");
    getchar();
    //return 0;
  }
  else
    ROS_INFO( "Succeed to open Biclops Communication\n" );

    //Homing sequnce at the bigining of the application
    biclops_obj.HomeAxes(axisMask,true);
    
    // Get shortcut references to each axis.
    panAxis = biclops_obj.GetAxis(Biclops::Pan);
    tiltAxis = biclops_obj.GetAxis(Biclops::Tilt);

    
    // Get the currently defined (default) motion profiles.
    panAxis->GetProfile(panProfile);
    tiltAxis->GetProfile(tiltProfile);
    double pitch = 0,roll = 0;
    
    
    while(ros::ok() ){
    
      panAxis->SetProfile(panProfile);
      panAxis->Move(false);
      tiltAxis->SetProfile(tiltProfile);
      tiltAxis->Move(false);
      
      spinOnce();
      
//       pan_joint_pos=(pan_pos_value/180)*3.14;
//       tilt_joint_pos=(tilt_pos_value/180)*3.14;  
//       joint_state.header.stamp = ros::Time::now();
//       joint_state.name.resize(2);
//       joint_state.position.resize(2);
//       joint_state.name[0] ="PAN_JOINT";
//       joint_state.position[0] = pan_joint_pos;
//       joint_state.name[1] ="TILT_JOINT";
//       joint_state.position[1] = -tilt_joint_pos;
//       
// //   send the joint state
//       joint_pub.publish(joint_state);
      loop_rate.sleep();
      
    }
    
    panAxis->DisableAmp();
    tiltAxis->DisableAmp();
    
    // Something went wrong. Return failure indication.
    return 0;
    
}

	    
